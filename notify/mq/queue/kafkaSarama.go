package queue

import (
	"github.com/IBM/sarama"
	"log"
	"net"
	"time"
)

type KafkaMQ struct {
	Producer         sarama.SyncProducer
	topicsOut        map[string]chan Message
	topics           []string
	brokers          []string
	responseChannels map[string]chan *sarama.ConsumerMessage
}

func NewKafkaMQ(broker, groupID string) (MessageQueuer, error) {
	waitStartKafka(broker)

	p, err := sarama.NewSyncProducer([]string{broker}, nil)
	if err != nil {
		return nil, err
	}

	return &KafkaMQ{
		Producer:         p,
		brokers:          []string{broker},
		responseChannels: make(map[string]chan *sarama.ConsumerMessage),
	}, nil
}

func waitStartKafka(addr string) {
	conn, err := net.DialTimeout("tcp", addr, 3*time.Second)
	if err != nil {
		log.Println("kafka s;eep", err)
		time.Sleep(3 * time.Second)
		waitStartKafka(addr)
	} else {
		log.Println("kafka rise!!!))")
		conn.Close()
	}
}

func (k *KafkaMQ) Publish(topic string, message []byte) error {
	msg := &sarama.ProducerMessage{
		Topic: topic,
		Value: sarama.ByteEncoder(message),
	}

	_, _, err := k.Producer.SendMessage(msg)

	return err
}

func (k *KafkaMQ) Subscribe(topic string) (<-chan Message, error) {
	c, err := sarama.NewConsumer(k.brokers, nil)
	if err != nil {
		return nil, err
	}

	partConsumer, err := c.ConsumePartition(topic, 0, sarama.OffsetNewest)
	out := make(chan Message)
	go func() {
		for {
			select {
			case msg, ok := <-partConsumer.Messages():
				if !ok {
					log.Println("Channel closed, exiting goroutine")
					return
				}
				out <- convertMessage(msg)
			}
		}
	}()

	return out, nil
}

func convertMessage(msg *sarama.ConsumerMessage) Message {
	return Message{
		Data: msg.Value,
	}
}

func (k *KafkaMQ) Ack(msg *Message) error {
	return nil
}

func (k *KafkaMQ) Close() error {
	return nil
}
