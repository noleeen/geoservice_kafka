package queue

//import (
//	"fmt"
//	"github.com/confluentinc/confluent-kafka-go/kafka"
//	"log"
//)
//
//type KafkaMQ struct {
//	Producer  *kafka.Producer
//	Consumer  *kafka.Consumer
//	topicsOut map[string]chan Message
//	topics    []string
//}
//
//type PartitionOffset struct {
//	Partition int32
//	Offset    kafka.Offset
//	Topic     *string
//}
//
//func NewKafkaMQ(broker, groupID string) (MessageQueuer, error) {
//	p, err := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": broker})
//	if err != nil {
//		return nil, err
//	}
//
//	c, err := kafka.NewConsumer(&kafka.ConfigMap{
//		"bootstrap.servers":    broker,
//		"auto.offset.reset":    "earliest",
//		"group.id":             groupID,
//		"max.poll.interval.ms": 12000,
//		"session.timeout.ms":   12000,
//	})
//	if err != nil {
//		return nil, err
//	}
//
//	topicsOut := make(map[string]chan Message)
//
//	go func() {
//		for {
//			var msg *kafka.Message
//			msg, err = c.ReadMessage(-1)
//			if err != nil {
//				for _, ch := range topicsOut {
//					ch <- Message{Err: err}
//				}
//				log.Print(err)
//				continue
//			}
//			topicsOut[*msg.TopicPartition.Topic] <- Message{
//				Topic: *msg.TopicPartition.Topic,
//				Data:  msg.Value,
//				// В Kafka offset может быть использован вместо DeliveryTag в RabbitMQ
//				Identity: msg.TopicPartition,
//			}
//		}
//	}()
//
//	return &KafkaMQ{
//		Producer:  p,
//		Consumer:  c,
//		topicsOut: topicsOut,
//	}, nil
//}
//
//func (k *KafkaMQ) Publish(topic string, message []byte) error {
//	msg := &kafka.Message{
//		TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: kafka.PartitionAny},
//		Value:          message,
//	}
//	return k.Producer.Produce(msg, nil)
//}
//
//func (k *KafkaMQ) Subscribe(topic string) (<-chan Message, error) {
//	k.topics = append(k.topics, topic)
//	err := k.Consumer.SubscribeTopics(k.topics, nil)
//	if err != nil {
//		return nil, err
//	}
//	out := make(chan Message)
//	k.topicsOut[topic] = out
//
//	return out, nil
//}
//
//func (k *KafkaMQ) Ack(msg *Message) error {
//	var err error
//	if v, ok := msg.Identity.(kafka.TopicPartition); ok {
//		_, err = k.Consumer.CommitOffsets([]kafka.TopicPartition{v})
//		return err
//	}
//
//	return fmt.Errorf("invalid identity type %T", msg.Identity)
//}
//
//func (k *KafkaMQ) Close() error {
//	k.Producer.Close()
//	return k.Consumer.Close()
//}
