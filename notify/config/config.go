package config

type Config struct {
	ServerRPC ServerRPC
	MQ        MessageQueuer
	Email     Email
}

func NewConfig() Config {
	return Config{
		ServerRPC: getServerRPCConfig(),
		MQ:        getMessageQueuerConfig(),
		Email:     getEmailConfig(),
	}
}
