package config

import "os"

type Email struct {
	From     string
	Port     string
	Host     string
	Password string
}

func getEmailConfig() Email {
	d := Email{}
	d.Port = os.Getenv("EMAIL_PORT")
	d.From = os.Getenv("EMAIL_FROM")
	d.Host = os.Getenv("EMAIL_HOST")
	d.Password = os.Getenv("EMAIL_PASSWORD")

	return d
}
