package provider

type SendIn struct {
	To    string
	From  string
	Title string
	Type  string
	Data  []byte
}
