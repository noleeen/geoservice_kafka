package provider

import (
	"fmt"
	"log"
	"net/smtp"
	"notify/config"
)

type Email struct {
	client smtp.Auth
	addr   string
	conf   config.Email
}

func NewEmail(conf config.Email) Sender {
	emailAuth := smtp.PlainAuth("", conf.From, conf.Password, conf.Host)
	return &Email{
		client: emailAuth,
		addr:   fmt.Sprintf("%s:%s", conf.Host, conf.Port),
		conf:   conf,
	}
}

func (e *Email) Send(in SendIn) error {
	emailBody := string(in.Data)
	contentType := in.Type

	mime := fmt.Sprintf("MIME-version: 1.0;\nContent-Type: %s; charset=\"UTF-8\";\n\n", contentType)
	subject := "Subject: " + in.Title + "\n"
	msg := []byte(subject + mime + "\n" + emailBody)

	if err := smtp.SendMail(e.addr, e.client, e.conf.From, []string{in.To}, msg); err != nil {
		log.Println("|Email.Send| SendMail err:", err)
		return err
	}
	return nil
}
