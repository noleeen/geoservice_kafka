package provider

type Sender interface {
	Send(in SendIn) error
}
