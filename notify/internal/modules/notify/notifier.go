package notify

import "notify/internal/entities/notifyEntity"

type Notifier interface {
	Push(in notifyEntity.PushIn) notifyEntity.PushOut
}
