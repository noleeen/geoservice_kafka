package notifyEntity

type PushIn struct {
	Identifier string
	Title      string
	Data       []byte
	Options    []interface{}
}

type PushOut struct {
	ErrorCode int
}
