package run

import (
	"log"
	"notify/config"
	"notify/internal/entities/notifyEntity"
	"notify/internal/errors"
	"notify/internal/modules/notify"
	"notify/internal/provider"
	"notify/mq"
	"time"
)

type App struct {
	conf config.Config
}

func NewApp(conf config.Config) *App {
	return &App{conf: conf}
}

func (a *App) Run() int {
	email := provider.NewEmail(a.conf.Email)

	notifier := notify.NewNotify(a.conf.Email, email)

	log.Println("| config kafka |", "\nport:", a.conf.MQ.KafkaPort, "\nhost:", a.conf.MQ.KafkaHost, "\ngroup:", a.conf.MQ.KafkaGroup)
	messageQueue, err := mq.NewMessageQueuer(a.conf)
	if err != nil {
		log.Fatal("| app | NewRabbitMQ error:", err)
	}
	defer messageQueue.Close()

	subscribe, err := messageQueue.Subscribe(a.conf.MQ.ExchangeName)
	if err != nil {
		log.Println("| app | Subscribe error:", err)
		return errors.SubscribeError
	}

	//userClientGRPC := userService.NewUserClientGRPC(a.conf)

	emailFor := "noleen@ya.ru"
	//var user userEntity.User

	ticker := time.NewTicker(600 * time.Second)
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			break
		case msg := <-subscribe:
			err = messageQueue.Ack(&msg)
			if err != nil {
				panic(err)
			}
			log.Println("|app| message from geo:", string(msg.Data))

			notifier.Push(notifyEntity.PushIn{
				Identifier: emailFor,
				Title:      "Превышение лимита",
				Data:       []byte("достигнут разрешённый лимит запросов в минуту. повторите запрос позже"),
			})
		default:
			time.Sleep(100 * time.Second)
		}
	}
	return errors.NoError
}
