package main

import (
	"log"
	"notify/config"
	"notify/internal/errors"
	"notify/run"
	"os"
)

func main() {
	conf := config.NewConfig()

	app := run.NewApp(conf)

	if err := app.Run(); err != errors.NoError {
		log.Println("|main_Notify| app run error:", err)
		os.Exit(2)
	}
}
