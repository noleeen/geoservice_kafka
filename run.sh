#!/bin/bash

echo "Creating network 'mylocal' if not exists..."
docker network create mylocal || true

echo "Starting services..."
docker-compose -f ./geo/docker-compose.yml up --build -d
docker-compose -f ./user/docker-compose.yml up --build -d
docker-compose -f ./auth/docker-compose.yml up --build -d
echo "Waiting..."
sleep 10
docker-compose -f ./notify/docker-compose.yml up --build -d
docker-compose -f ./proxy/docker-compose.yml up --build -d
