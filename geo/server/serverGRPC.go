package server

import (
	"context"
	"fmt"
	"geo/config"
	"google.golang.org/grpc"
	"log"
	"net"
)

type ServerRPC struct {
	conf config.ServerRPC
	srv  *grpc.Server
}

func NewServerRPC(conf config.ServerRPC, srv *grpc.Server) Serverer {
	return &ServerRPC{
		conf: conf,
		srv:  srv,
	}
}

func (s *ServerRPC) Serve(ctx context.Context) error {
	var err error

	chErr := make(chan error)
	go func() {
		var l net.Listener
		l, err = net.Listen("tcp", fmt.Sprintf(":%s", s.conf.Port))
		if err != nil {
			log.Println("error geo|Serve|ServerGRPC:", err)
			chErr <- err
		}

		log.Print("gprc server started on port:", s.conf.Port)
		if err = s.srv.Serve(l); err != nil {
			chErr <- err
		}
	}()

	select {
	case <-chErr:
		return err
	case <-ctx.Done():
		s.srv.GracefulStop()
	}
	return err

}
