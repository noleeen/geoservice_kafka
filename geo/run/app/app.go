package run

import (
	"context"
	"database/sql"
	"fmt"
	"geo/config"
	"geo/grpcGeo"
	"geo/internal/modules/geo/service"
	"geo/mq"
	"geo/server"
	_ "github.com/lib/pq"
	"github.com/redis/go-redis/v9"
	pb "gitlab.com/noleeen/protogeo/gen/geo"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"log"
	"net/http"
	"os"
)

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() error
}

// App - структура приложения
type App struct {
	Conf config.Config
	Rpc  server.Serverer
	Sig  chan os.Signal
}

func NewApp(conf config.Config) *App {
	return &App{
		Conf: conf,
		Sig:  make(chan os.Signal, 1),
	}
}

// Run - запуск приложения
func (a *App) Run() error {
	ctx, cancel := context.WithCancel(context.Background())
	errGroup, ctx := errgroup.WithContext(ctx)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		log.Println("signal interrupt received", sigInt)
		cancel()
		return nil
	})

	// запускаем http сервер
	errGroup.Go(func() error {
		err := a.Rpc.Serve(ctx)
		if err != nil && err != http.ErrServerClosed {
			log.Fatal(err)
			return err
		}
		return nil
	})

	if err := errGroup.Wait(); err != nil {
		return err
	}

	return nil

}

func (a *App) Bootstrap(options ...interface{}) Runner {

	//postgresql
	dsn := a.Conf.GetDsnDB()
	fmt.Println("dsn:", dsn)
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		fmt.Println("error sql.Open")
		log.Fatal(err)
	}

	ctx := context.Background()

	//вот здесь запустим редис для быстрого кэширования запросов
	clientRedis := redis.NewClient(&redis.Options{
		Addr: "redis:6379",
		DB:   0,
	})
	// Проверка соединения с Redis
	pong, err := clientRedis.Ping(ctx).Result()
	if err != nil {
		fmt.Println("Ошибка соединения с Redis:", err)
		log.Println(err)
	}
	fmt.Println("Соединение с Redis успешно:", pong)

	log.Println("| config kafka |", "\nport:", a.Conf.MQ.KafkaPort, "\nhost:", a.Conf.MQ.KafkaHost, "\ngroup:", a.Conf.MQ.KafkaGroup)
	messageQueue, err := mq.NewMessageQueuer(a.Conf)
	if err != nil {
		log.Fatal("| app | NewRabbitMQ error:", err)
	}
	defer messageQueue.Close()

	geoService2 := service.NewGeoService(db, clientRedis, &a.Conf, messageQueue)

	serverGRPC := grpc.NewServer()

	a.Rpc = server.NewServerRPC(a.Conf.ServerRPC, serverGRPC)
	geoGRPC := grpcGeo.NewGeoServiceGRPC(geoService2)
	pb.RegisterGeorerServer(serverGRPC, geoGRPC)

	return a
}
