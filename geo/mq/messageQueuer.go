package mq

import (
	"fmt"
	"geo/config"
	"geo/mq/queue"
	"github.com/streadway/amqp"
	"log"
)

func NewMessageQueuer(conf config.Config) (queue.MessageQueuer, error) {
	switch conf.MQ.Type {
	case "rabbit":
		return newRabbitMQ(conf)
	case "kafka":
		return newKafkaMQ(conf)
	default:
		return nil, fmt.Errorf("| NewMessageQueuer error | invalid MQ_TYPE")
	}
}

func newKafkaMQ(conf config.Config) (queue.MessageQueuer, error) {
	kafka := fmt.Sprintf("%s:%s", conf.MQ.KafkaHost, conf.MQ.KafkaPort)
	return queue.NewKafkaMQ(kafka, conf.MQ.KafkaGroup)
}

func newRabbitMQ(conf config.Config) (queue.MessageQueuer, error) {
	url := fmt.Sprintf("amqp://guest:guest@%s:%s/", conf.MQ.RabbitHost, conf.MQ.RabbitPort)
	conn, err := amqp.Dial(url)
	if err != nil {
		log.Printf("error to connection RabbitMQ: %v", err)
		return nil, err
	}
	mq, err := queue.NewRabbitMQ(conn)
	if err != nil {
		log.Printf("error to new rabbbitMQ: %v", err)
		return nil, err
	}

	if err2 := queue.CreateExchange(conn, conf.MQ.ExchangeName, conf.MQ.ExchangeType); err2 != nil {
		log.Printf("error to create exchange: %v", err)
		return nil, err
	}

	return mq, nil
}
