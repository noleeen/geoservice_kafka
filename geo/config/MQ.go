package config

import "os"

type MessageQueuer struct {
	Type         string
	RabbitPort   string
	RabbitHost   string
	KafkaPort    string
	KafkaHost    string
	KafkaGroup   string
	ExchangeName string
	ExchangeType string
}

func getMessageQueuerConfig() MessageQueuer {
	d := MessageQueuer{}

	d.Type = os.Getenv("MQ_TYPE")

	d.RabbitPort = os.Getenv("RABBIT_PORT")
	d.RabbitHost = os.Getenv("RABBIT_HOST")

	d.KafkaPort = os.Getenv("KAFKA_PORT")
	d.KafkaHost = os.Getenv("KAFKA_HOST")
	d.KafkaGroup = os.Getenv("KAFKA_GROUP")

	d.ExchangeName = os.Getenv("EXCHANGE_NAME")
	d.ExchangeType = os.Getenv("EXCHANGE_TYPE")

	return d
}
