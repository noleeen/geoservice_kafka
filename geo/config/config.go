package config

import (
	"fmt"
	"os"
)

type Config struct {
	DB        DB
	DDKeys    DadataKeys
	ServerRPC ServerRPC
	MQ        MessageQueuer
}

func NewConfig() Config {
	return Config{
		DB:        getDbConfig(),
		DDKeys:    getDadataKeysConfig(),
		ServerRPC: getServerRPCConfig(),
		MQ:        getMessageQueuerConfig(),
	}
}

func (c *Config) GetDsnDB() string {
	return fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		c.DB.Host, c.DB.Port, c.DB.Username, c.DB.Password, c.DB.DbName, c.DB.SslMode)
}

func getEnv(key, fallback string) string {
	if val, ok := os.LookupEnv(key); ok {
		return val
	}
	return fallback
}
