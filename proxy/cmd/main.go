package main

import (
	"log"
	"os"
	"proxy/config"
	"proxy/run/app"
)

func main() {
	conf := config.NewConfig()

	app := run.NewApp(conf)

	if err := app.Bootstrap().Run(); err != nil {
		log.Println("|main_Auth| app run error:", err)
		os.Exit(2)
	}
}
