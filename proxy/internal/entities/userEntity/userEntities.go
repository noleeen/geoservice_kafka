package userEntity

type User struct {
	Id       int    `json:"id"`
	Name     string `json:"name"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

type ProfileRequest struct {
	Email string `json:"email"`
}
