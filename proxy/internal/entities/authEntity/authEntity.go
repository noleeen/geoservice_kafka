package authEntity

type RegisterRequest struct {
	Name     string `json:"name"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

type RegisterResponse struct {
	ErrorStatus int    `json:"error_status"`
	Message     string `json:"message"`
}

type LoginRequest struct {
	Name     string `json:"name"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

type LoginResponse struct {
	Name        string `json:"name"`
	AccessToken string `json:"access_token"`
	Message     string `json:"message"`
	ErrorStatus int    `json:"error_status"`
}
