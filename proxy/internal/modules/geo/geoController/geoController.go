package geoController

import "net/http"

type GeoControllerer interface {
	SearchAddressHandler(w http.ResponseWriter, r *http.Request)
	GeocodeAddressHandler(w http.ResponseWriter, r *http.Request)
}
