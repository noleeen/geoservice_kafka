package geoController

import (
	"encoding/json"
	"net/http"
	"proxy/internal/entities/geoEntity"
	"proxy/internal/modules/geo/geoService"
	"proxy/responder"
)

type GeoController struct {
	service geoService.GeoServicer
	respond responder.Responder
}

func NewGeoController(service geoService.GeoServicer, respond responder.Responder) GeoControllerer {
	return &GeoController{
		service: service,
		respond: respond,
	}
}

func (g *GeoController) SearchAddressHandler(w http.ResponseWriter, r *http.Request) {

	var requestByUser geoEntity.SearchRequest
	err := json.NewDecoder(r.Body).Decode(&requestByUser)
	if err != nil {
		g.respond.ErrorBadRequest(w, err)
		return
	}

	searchResponse, err := g.service.PrepareSearchRequest(requestByUser)
	if err != nil {
		g.respond.ErrorInternal(w, err)
		return
	}

	g.respond.OutputJSON(w, searchResponse)
}

func (g *GeoController) GeocodeAddressHandler(w http.ResponseWriter, r *http.Request) {

	var request geoEntity.GeocodeRequest
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		g.respond.ErrorBadRequest(w, err)
		return
	}

	suggestions, err := g.service.PrepareGeocodeRequest(request)
	if err != nil {
		g.respond.ErrorInternal(w, err)
		return
	}

	addresses := &geoEntity.GeocodeResponse{}

	if suggestions.Message != "" {
		addresses.Addresses = []*geoEntity.Address{{
			Result: suggestions.Message,
		}}
	} else {

		for _, adr := range suggestions.Suggestions {
			addresses.Addresses = append(addresses.Addresses,
				&geoEntity.Address{
					Lat:    adr.Data["geo_lat"],
					Lon:    adr.Data["geo_lon"],
					Result: adr.Value,
				})
		}
	}

	g.respond.OutputJSON(w, addresses)
}
