package geoService

import (
	"context"
	"fmt"
	pb "gitlab.com/noleeen/protogeo/gen/geo"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
	"proxy/config"
	"proxy/internal/entities/geoEntity"
)

type GeoClientGRPC struct {
	client pb.GeorerClient
}

func NewGeoClientGRPC(conf config.Config) *GeoClientGRPC {

	clientConn, err := grpc.Dial(fmt.Sprintf("%s:%s", conf.ServerRPC.GeoHost, conf.ServerRPC.Port), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatal("|geo| grpc.Dial | error init grpc client ", err)
	}

	client := pb.NewGeorerClient(clientConn)
	log.Println("grpc geoClient init")

	return &GeoClientGRPC{client: client}
}

func (g *GeoClientGRPC) PrepareGeocodeRequest(in geoEntity.GeocodeRequest) (*geoEntity.Suggestions, error) {
	// Преобразование geoEntity.GeocodeRequest в pb.GeocodeRequest
	req := &pb.GeocodeRequest{
		Lat: in.Lat,
		Lng: in.Lng,
	}

	// Вызов метода GeocodeSearch на сервере
	geocodeResponse, err := g.client.PrepareGeocodeRequest(context.Background(), req) //ПЛОХО!!! TODO контекст изначально должен быть в методах сервиса!!!
	if err != nil {
		log.Println("|g.client.PrepareGeocodeRequest| error:", err)
		return nil, err
	}

	if geocodeResponse.Message != "" {
		return &geoEntity.Suggestions{Message: geocodeResponse.Message}, nil
	}

	// Преобразование pb.Suggestions в geoEntity.Suggestions
	var s []*geoEntity.Suggestion
	for _, val := range geocodeResponse.Suggestions {
		s = append(s, &geoEntity.Suggestion{
			Value: val.Value,
			Data:  val.Data,
		})
	}

	return &geoEntity.Suggestions{Suggestions: s}, nil
}
func (g *GeoClientGRPC) PrepareSearchRequest(in geoEntity.SearchRequest) (*geoEntity.SearchResponse, error) {
	// Преобразование geoEntity.SearchRequest в pb.SearchRequest
	req := &pb.SearchRequest{Query: in.Query}

	// Вызов метода SearchAddress на сервере
	searchResponse, err := g.client.PrepareSearchRequest(context.Background(), req) //ПЛОХО!!! TODO контекст изначально должен быть в методах сервиса!!!
	if err != nil {
		log.Println("|g.client.PrepareSearchRequest| error:", err)
		return nil, err
	}

	if searchResponse.Message != "" {
		return &geoEntity.SearchResponse{Message: searchResponse.Message}, nil
	}

	// Преобразование pb.SearchResponse в geoEntity.SearchResponse
	var a []*geoEntity.Address

	for _, val := range searchResponse.Addresses {
		a = append(a, &geoEntity.Address{
			Lat:    val.Lat,
			Lon:    val.Lon,
			Result: val.Result,
		})
	}

	return &geoEntity.SearchResponse{Addresses: a}, nil
}
