package authController

import (
	"encoding/json"
	"net/http"
	"proxy/internal/entities/authEntity"
	"proxy/internal/modules/auth/authService"
	"proxy/responder"
)

type AuthController struct {
	service   authService.AuthServicer
	responder responder.Responder
}

func NewAuthController(service authService.AuthServicer, respond responder.Responder) AuthControllerer {
	return &AuthController{
		service:   service,
		responder: respond,
	}
}

func (a *AuthController) Register(w http.ResponseWriter, r *http.Request) {
	var request authEntity.RegisterRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		a.responder.ErrorBadRequest(w, err)
		return
	}

	register := a.service.Register(r.Context(), request)

	a.responder.OutputJSON(w, register)
}
func (a *AuthController) Login(w http.ResponseWriter, r *http.Request) {
	var request authEntity.LoginRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		a.responder.ErrorBadRequest(w, err)
		return
	}

	register := a.service.Login(r.Context(), request)

	a.responder.OutputJSON(w, register)
}
