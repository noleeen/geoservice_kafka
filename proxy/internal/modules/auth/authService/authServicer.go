package authService

import (
	"context"
	"proxy/internal/entities/authEntity"
)

type AuthServicer interface {
	Register(ctx context.Context, in authEntity.RegisterRequest) authEntity.RegisterResponse
	Login(ctx context.Context, in authEntity.LoginRequest) authEntity.LoginResponse
}
