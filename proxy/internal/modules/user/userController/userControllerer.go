package userController

import (
	"net/http"
)

type UserControllerer interface {
	Profile(w http.ResponseWriter, r *http.Request)
	List(w http.ResponseWriter, r *http.Request)
}
