package userService

import (
	"context"
	"proxy/internal/entities/userEntity"
)

type UserServicer interface {
	Profile(ctx context.Context, email string) (userEntity.User, error)
	List(ctx context.Context) ([]userEntity.User, error)
}
