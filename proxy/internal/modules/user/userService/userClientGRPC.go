package userService

import (
	"context"
	"fmt"
	pb "gitlab.com/noleeen/protogeo/gen/user"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
	"proxy/config"
	"proxy/internal/entities/userEntity"
)

type UserClientGRPC struct {
	client pb.UserServiceGRPCClient
}

func NewUserClientGRPC(conf config.Config) *UserClientGRPC {
	clientConn, err := grpc.Dial(fmt.Sprintf("%s:%s", conf.ServerRPC.UserHost, conf.ServerRPC.Port), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatal("|user| grpc.Dial | error init grpc client ", err)
	}

	client := pb.NewUserServiceGRPCClient(clientConn)
	log.Println("grpc userClient init")
	return &UserClientGRPC{client: client}
}

func (u *UserClientGRPC) Profile(ctx context.Context, email string) (userEntity.User, error) {
	response, err := u.client.Profile(ctx, &pb.ProfileRequest{Email: email})
	if err != nil {
		return userEntity.User{}, err
	}
	user := userEntity.User{
		Name:  response.Name,
		Email: response.Email,
	}

	return user, nil
}
func (u *UserClientGRPC) List(ctx context.Context) ([]userEntity.User, error) {
	list, err := u.client.List(ctx, &pb.ListRequest{})
	if err != nil {
		return nil, err
	}
	userList := make([]userEntity.User, 0, len(list.Users))
	for _, v := range list.Users {
		user := userEntity.User{
			Name:  v.Name,
			Email: v.Email,
		}
		userList = append(userList, user)
	}

	return userList, nil
}
