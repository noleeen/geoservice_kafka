package modules

import (
	"proxy/config"
	"proxy/internal/modules/auth/authService"
	"proxy/internal/modules/geo/geoService"
	"proxy/internal/modules/user/userService"
)

type Services struct {
	UserService userService.UserServicer
	AuthService authService.AuthServicer
	GeoService  geoService.GeoServicer
}

func NewServices(config config.Config) *Services {
	uService := userService.NewUserClientGRPC(config)
	aService := authService.NewAuthClientGRPC(config)
	gService := geoService.NewGeoClientGRPC(config)

	return &Services{
		UserService: uService,
		AuthService: aService,
		GeoService:  gService,
	}
}
