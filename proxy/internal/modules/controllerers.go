package modules

import (
	"proxy/internal/modules/auth/authController"
	"proxy/internal/modules/geo/geoController"
	"proxy/internal/modules/user/userController"
	"proxy/responder"
)

type Controllers struct {
	UserController userController.UserControllerer
	AuthController authController.AuthControllerer
	GeoController  geoController.GeoControllerer
}

func NewControllers(services Services, respond responder.Responder) *Controllers {
	uController := userController.NewUserController(services.UserService, respond)
	aController := authController.NewAuthController(services.AuthService, respond)
	gController := geoController.NewGeoController(services.GeoService, respond)

	return &Controllers{
		UserController: uController,
		AuthController: aController,
		GeoController:  gController,
	}
}
