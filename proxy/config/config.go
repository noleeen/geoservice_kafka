package config

type Config struct {
	ServerRPC  ServerRPC
	ServerHttp ServerHttp
	Auth       Auth
}

func NewConfig() Config {
	return Config{
		ServerHttp: getServerHttpConfig(),
		ServerRPC:  getServerRPCConfig(),
		Auth:       getAuthConfig(),
	}
}
