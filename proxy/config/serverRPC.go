package config

import "os"

type ServerRPC struct {
	Port     string
	UserHost string
	AuthHost string
	GeoHost  string
}

func getServerRPCConfig() ServerRPC {
	d := ServerRPC{}
	d.UserHost = os.Getenv("USER_RPC_HOST")
	d.AuthHost = os.Getenv("AUTH_RPC_HOST")
	d.GeoHost = os.Getenv("GEO_RPC_HOST")
	d.Port = os.Getenv("RPC_PORT")

	return d
}
