package config

import "os"

type Auth struct {
	TokenSecret string
}

func getAuthConfig() Auth {
	d := Auth{}
	d.TokenSecret = os.Getenv("TOKEN_SECRET")

	return d
}
