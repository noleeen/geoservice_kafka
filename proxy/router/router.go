package router

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/jwtauth"
	"net/http"
	"proxy/internal/modules"
	"proxy/static"
)

func NewApiRouter(r *chi.Mux, controllers *modules.Controllers, token *jwtauth.JWTAuth) http.Handler {

	r.Get("/swagger", static.SwaggerUI)
	r.Get("/static/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))).ServeHTTP(w, r)
	})

	r.Route("/api", func(r chi.Router) {

		r.Post("/login", controllers.AuthController.Login)
		r.Post("/register", controllers.AuthController.Register)
		r.Route("/address", func(r chi.Router) {
			r.Use(jwtauth.Verifier(token))
			r.Use(jwtauth.Authenticator)

			r.Post("/search", controllers.GeoController.SearchAddressHandler)
			r.Post("/geocode", controllers.GeoController.GeocodeAddressHandler)

		})
		r.Post("/user/profile", controllers.UserController.Profile)
		r.Get("/user/list", controllers.UserController.List)
	})

	return r
}
