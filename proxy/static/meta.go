// Package static Geocode API.
//
//	 Шапка ты где?!
//
//		Version: 1.0.0
//		Schemes: http, https
//		BasePath: /
//
//		Consumes:
//		- application/json
//	 - multipart/form-data
//
//		Produces:
//		- application/json
//
//		Security:
//		- Bearer
//
//		SecurityDefinitions:
//		  Bearer:
//		    type: apiKey
//		    name: Authorization
//		    in: header
//
// swagger:meta
package static

//go:generate swagger generate spec -o ./swagger.json --scan-models
