package static

//swagger:route POST /api/register registrationUser RegisterRequest
// Регистрация нового пользователя
// security:
// - basic
// Responses:
// 200:

//swagger:parameters RegisterRequest
type RegisterRequest struct {
	//R
	//in:body
	//required: true
	//example: {"name":"testUser", "email":"testEmail", "password":"testPass"}
	Reg string
}

// swagger:route POST /api/login loginUser LoginRequest
// Аутентификация пользователя
// security:
// - basic
//Responses:
// 200:

//swagger:parameters LoginRequest
type LoginRequest struct {
	//L
	//in:body
	//required: true
	//example: {"email":"testEmail", "password":"testPass"}
	Body string
}
