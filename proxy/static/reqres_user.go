package static

import "proxy/internal/entities/userEntity"

// swagger:route POST /api/user/profile user ProfileRequest
// Получение информации о пользователе.
// security:
//   - Bearer: []
// responses:
//  200: ProfileResponse

// swagger:parameters ProfileRequest
type ProfileRequest struct {
	// in:body
	// required: true
	//example: {"email":"testEmail"}
	Body userEntity.ProfileRequest
}

// swagger:response ProfileResponse
type ProfileResponse struct {
	// in:body
	Body userEntity.User
}

// swagger:route GET /api/user/list user ListRequest
// Get list users.
// security:
//   - Bearer: []
// responses:
//  200: ListResponse

// swagger:response ListResponse
type ListResponse struct {
	// in:body
	List []userEntity.User
}
