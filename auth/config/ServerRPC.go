package config

import "os"

type ServerRPC struct {
	Port string
	Host string
}

func getServerRPCConfig() ServerRPC {
	d := ServerRPC{}
	d.Host = os.Getenv("USER_GRPC_HOST")
	d.Port = os.Getenv("GRPC_PORT")

	return d
}
