package userGRPC

import (
	authEntity "auth/internal/entities"
	"context"
)

type UserClienter interface {
	Profile(ctx context.Context, email string) (authEntity.User, error)
	Create(ctx context.Context, user authEntity.User) error
}
