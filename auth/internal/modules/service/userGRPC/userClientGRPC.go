package userGRPC

import (
	"auth/config"
	authEntity "auth/internal/entities"
	"context"
	"fmt"
	pb "gitlab.com/noleeen/protogeo/gen/user"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
)

type UserClientGRPC struct {
	client pb.UserServiceGRPCClient
}

func NewUserClientGRPC(conf config.Config) UserClienter {
	clientConn, err := grpc.Dial(fmt.Sprintf("%s:%s", conf.ServerRPC.Host, conf.ServerRPC.Port),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatal("| grpcAuth.Dial | error init grpcAuth client ", err)
	}
	client := pb.NewUserServiceGRPCClient(clientConn)
	log.Println("grpcAuth client init")
	return &UserClientGRPC{client: client}
}

func (u *UserClientGRPC) Profile(ctx context.Context, email string) (authEntity.User, error) {
	resp, err := u.client.Profile(ctx, &pb.ProfileRequest{Email: email})
	if err != nil {
		log.Println("|auth| UserGRPCClient.Profile err:", err)
		return authEntity.User{}, err
	}
	user := authEntity.User{
		Id:       int(resp.Id),
		Name:     resp.Name,
		Email:    resp.Email,
		Password: resp.Password,
	}

	return user, nil
}

func (u *UserClientGRPC) Create(ctx context.Context, user authEntity.User) error {
	_, err := u.client.Create(ctx, &pb.CreateRequest{
		Name:     user.Name,
		Email:    user.Email,
		Password: user.Password,
	})
	if err != nil {
		log.Println("|auth| UserGRPCClient.Create err:", err)
		return err
	}
	return nil
}
