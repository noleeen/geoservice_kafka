package errNum

const (
	NoError = iota
	ErrorHashPassword
	ErrorUserAlreadyExists
	ErrorUserNotFound
	PasswordError
	GenerateTokenError
	InternalError
)
