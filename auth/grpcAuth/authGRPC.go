package grpcAuth

import (
	authEntity "auth/internal/entities"
	"auth/internal/modules/service/auth"
	"context"
	pb "gitlab.com/noleeen/protogeo/gen/auth"
)

type AuthGRPC struct {
	service auth.AuthServicer
	pb.UnimplementedAutherServer
}

func NewAuthGRPC(service auth.AuthServicer) *AuthGRPC {
	return &AuthGRPC{service: service}
}

func (a *AuthGRPC) Register(ctx context.Context, in *pb.RegisterRequest) (*pb.RegisterResponse, error) {
	resp := a.service.Register(ctx, authEntity.RegisterRequest{
		Name:     in.Name,
		Email:    in.Email,
		Password: in.Password,
	})

	return &pb.RegisterResponse{
		Status:  uint32(resp.ErrorStatus),
		Message: resp.Message,
	}, nil

}
func (a *AuthGRPC) Login(ctx context.Context, in *pb.LoginRequest) (*pb.LoginResponse, error) {
	resp := a.service.Login(ctx, authEntity.LoginRequest{
		Email:    in.Email,
		Password: in.Password,
	})

	return &pb.LoginResponse{
		Name:    resp.Name,
		Token:   resp.AccessToken,
		Status:  uint32(resp.ErrorStatus),
		Message: resp.Message,
	}, nil
}
