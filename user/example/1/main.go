package main

import (
	"github.com/streadway/amqp"
	"log"
)

func main() {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	if err != nil {
		log.Fatal("don-t connection")
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		log.Fatal("don-t create chanel")
	}
	defer ch.Close()

	queue, err := ch.QueueDeclare(
		"my_queue",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Fatal("don-t declarate queue", err)
	}

	err = ch.Publish(
		"",
		queue.Name,
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte("Ghbdtn, vbh!"),
		},
	)

	if err != nil {
		log.Fatalf("Не удалось отправить сообщение в очередь: %v", err)
	}

	log.Println("Сообщение успешно отправлено в очередь")

}
