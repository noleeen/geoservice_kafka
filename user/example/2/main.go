package main

import (
	"log"
	"sync"

	"github.com/streadway/amqp"
)

func main() {
	// Установка соединения с RabbitMQ
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	if err != nil {
		log.Fatalf("Не удалось установить соединение с RabbitMQ: %v", err)
	}
	defer conn.Close()

	// Создание канала
	ch, err := conn.Channel()
	if err != nil {
		log.Fatalf("Не удалось создать канал: %v", err)
	}
	defer ch.Close()

	// Объявление очереди
	queue, err := ch.QueueDeclare(
		"my_queue", // Имя очереди
		false,      // Долговременность очереди
		false,      // Автоматическое удаление очереди при отключении последнего потребителя
		false,      // Исключительность очереди (доступ только для текущего соединения)
		false,      // Не ждать подтверждения от брокера при создании очереди
		nil,        // Дополнительные аргументы
	)
	if err != nil {
		log.Fatalf("Не удалось объявить очередь: %v", err)
	}

	// Количество отправителей и получателей
	numProducers := 3
	numConsumers := 2

	// Ожидание завершения всех отправителей и получателей
	var wg sync.WaitGroup
	wg.Add(numProducers + numConsumers)

	// Запуск отправителей
	for i := 0; i < numProducers; i++ {
		go func(id int) {
			defer wg.Done()

			// Отправка сообщения в очередь
			err := ch.Publish(
				"",         // Обменник (пустая строка - использование обменника по умолчанию)
				queue.Name, // Имя очереди
				false,      // Не ждать подтверждения от брокера при отправке сообщения
				false,      // Не сохранять сообщение при перезапуске брокера
				amqp.Publishing{
					ContentType: "text/plain",
					Body:        []byte("Привет, мир!"),
				},
			)
			if err != nil {
				log.Printf("Ошибка при отправке сообщения от отправителя %d: %v", id, err)
			} else {
				log.Printf("Отправитель %d успешно отправил сообщение в очередь", id)
			}
		}(i)
	}

	// Запуск получателей
	for i := 0; i < numConsumers; i++ {
		go func(id int) {
			defer wg.Done()

			// Получение сообщения из очереди
			msgs, err := ch.Consume(
				queue.Name, // Имя очереди
				"",         // Имя потребителя (пустая строка - использование случайного имени)
				true,       // Автоматическое подтверждение получения сообщения
				false,      // Не исключительная блокировка (другие потребители могут получать сообщения из очереди)
				false,      // Не ждать подтверждения от брокера при регистрации потребителя
				false,      // Не использовать дополнительные аргументы
				nil,        // Дополнительные аргументы
			)
			if err != nil {
				log.Printf("Ошибка при регистрации получателя %d: %v", id, err)
				return
			}

			// Обработка полученных сообщений
			for msg := range msgs {
				log.Printf("Получатель %d получил сообщение: %s", id, msg.Body)
			}
		}(i)
	}

	// Ожидание завершения всех отправителей и получателей
	wg.Wait()
}
