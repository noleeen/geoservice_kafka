package grpc

import (
	"context"
	pb "gitlab.com/noleeen/protogeo/gen/user"
	"user/internal/entities/userEntity"
	"user/internal/modules/user/userService"
)

type UserGRPC struct {
	service userService.UserServicer
	pb.UnimplementedUserServiceGRPCServer
}

func NewUserGRPC(service userService.UserServicer) *UserGRPC {
	return &UserGRPC{service: service}
}

func (u *UserGRPC) Create(ctx context.Context, in *pb.CreateRequest) (*pb.CreateResponse, error) {

	err := u.service.NewUser(ctx, userEntity.User{
		Name:     in.Name,
		Email:    in.Email,
		Password: in.Password,
	})
	if err != nil {
		return &pb.CreateResponse{Success: false}, err
	}
	return &pb.CreateResponse{Success: true}, nil
}

func (u *UserGRPC) UserById(ctx context.Context, in *pb.UserByIdRequest) (*pb.UserByIdResponse, error) {

	user, err := u.service.UserById(ctx, int(in.Id))
	if err != nil {
		return nil, err
	}
	userProto := &pb.UserByIdResponse{
		Id:       uint32(user.Id),
		Name:     user.Name,
		Email:    user.Email,
		Password: user.Password,
	}

	return userProto, nil
}

func (u *UserGRPC) Profile(ctx context.Context, in *pb.ProfileRequest) (*pb.ProfileResponse, error) {
	user, err := u.service.UserByEmail(ctx, in.Email)
	if err != nil {
		return nil, err
	}

	userProto := &pb.ProfileResponse{
		Id:       uint32(user.Id),
		Name:     user.Name,
		Email:    user.Email,
		Password: user.Password,
	}

	return userProto, nil
}

func (u *UserGRPC) List(ctx context.Context, in *pb.ListRequest) (*pb.ListResponse, error) {
	list, err := u.service.UsersList(ctx)
	if err != nil {
		return nil, err
	}

	usersList := &pb.ListResponse{}
	for _, val := range list {
		user := &pb.ProfileResponse{
			Id:    uint32(val.Id),
			Name:  val.Name,
			Email: val.Email,
		}
		usersList.Users = append(usersList.Users, user)

	}
	return usersList, nil
}
