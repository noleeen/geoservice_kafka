package config

import "os"

type Server struct {
	Port string
}

func getServerConfig() Server {
	d := Server{}
	d.Port = os.Getenv("SERVER_PORT")
	return d
}
