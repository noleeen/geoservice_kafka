package config

import "os"

type ServerRPC struct {
	Port string
	Type string
}

func getServerRPCConfig() ServerRPC {
	d := ServerRPC{}
	//d.Type = os.Getenv("RPC_PROTOCOL")
	d.Port = os.Getenv("GRPC_PORT_USER")

	return d
}
