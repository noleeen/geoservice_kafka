package userStorage

import (
	"context"
	"user/internal/entities/userEntity"
)

type UserStorager interface {
	CreateUser(ctx context.Context, user userEntity.User) error
	GetUserById(ctx context.Context, id int) (userEntity.User, error)
	GetUserByEmail(ctx context.Context, email string) (userEntity.User, error)
	GetUsersList(ctx context.Context) ([]userEntity.User, error)
}
