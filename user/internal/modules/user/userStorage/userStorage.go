package userStorage

import (
	"context"
	"database/sql"
	"log"
	"user/internal/entities/userEntity"
)

type UserStorage struct {
	db *sql.DB
}

func NewUserStorage(db *sql.DB) UserStorager {
	return &UserStorage{db: db}
}

func (u *UserStorage) CreateUser(ctx context.Context, user userEntity.User) error {
	q := "INSERT INTO users(name,email,password)values($1,$2,$3)"
	_, err := u.db.ExecContext(ctx, q, user.Name, user.Email, user.Password)
	if err != nil {
		log.Println("error CreateUser:", err)
		return err
	}
	return nil
}

func (u *UserStorage) GetUserById(ctx context.Context, id int) (userEntity.User, error) {
	q := "SELECT id, name, email, password FROM users WHERE id = $1"
	var user userEntity.User

	err := u.db.QueryRowContext(ctx, q, id).Scan(&user.Id, &user.Name, &user.Email, &user.Password)
	if err != nil {
		log.Println("error GetUserById:", err)
		return userEntity.User{}, err
	}
	return user, nil
}

func (u *UserStorage) GetUserByEmail(ctx context.Context, email string) (userEntity.User, error) {
	q := "SELECT id, name, email, password FROM users WHERE email = $1"
	var user userEntity.User

	err := u.db.QueryRowContext(ctx, q, email).Scan(&user.Id, &user.Name, &user.Email, &user.Password)
	if err != nil {
		log.Println("error GetUserByEmail:", err)
		return userEntity.User{}, err
	}
	return user, nil
}

func (u *UserStorage) GetUsersList(ctx context.Context) ([]userEntity.User, error) {
	q := "SELECT * FROM users"
	rows, err := u.db.QueryContext(ctx, q)
	if err != nil {
		log.Println("error GetUsersList:", err)
		return nil, err
	}

	var users []userEntity.User
	for rows.Next() {
		var user userEntity.User
		if err = rows.Scan(&user.Id, &user.Name, &user.Email, &user.Password); err != nil {
			return nil, err
		}
		users = append(users, user)
	}

	return users, nil
}
