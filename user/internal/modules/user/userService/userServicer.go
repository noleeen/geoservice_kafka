package userService

import (
	"context"
	"user/internal/entities/userEntity"
)

type UserServicer interface {
	NewUser(ctx context.Context, user userEntity.User) error
	UserById(ctx context.Context, id int) (userEntity.User, error)
	UserByEmail(ctx context.Context, email string) (userEntity.User, error)
	UsersList(ctx context.Context) ([]userEntity.User, error)
}
