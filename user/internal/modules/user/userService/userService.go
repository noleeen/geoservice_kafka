package userService

import (
	"context"
	"database/sql"
	"user/internal/entities/userEntity"
	"user/internal/modules/user/userStorage"
)

type UserService struct {
	storage userStorage.UserStorager
}

func NewUserService(db *sql.DB) UserServicer {
	return &UserService{storage: userStorage.NewUserStorage(db)}
}

func (uS *UserService) NewUser(ctx context.Context, user userEntity.User) error {
	return uS.storage.CreateUser(ctx, user)
}

func (uS *UserService) UserById(ctx context.Context, id int) (userEntity.User, error) {
	return uS.storage.GetUserById(ctx, id)
}

func (uS *UserService) UserByEmail(ctx context.Context, email string) (userEntity.User, error) {
	return uS.storage.GetUserByEmail(ctx, email)
}

func (uS *UserService) UsersList(ctx context.Context) ([]userEntity.User, error) {
	return uS.storage.GetUsersList(ctx)
}
