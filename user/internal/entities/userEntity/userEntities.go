package userEntity

type User struct {
	Id       int
	Name     string
	Email    string
	Password string
}
